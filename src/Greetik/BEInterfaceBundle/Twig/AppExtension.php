<?php

namespace Greetik\BEInterfaceBundle\Twig;

class AppExtension extends \Twig_Extension {

    private $beinterface;

    public function __construct($_beinterface) {
        $this->beinterface = $_beinterface;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('age', array($this, 'getAge')), //Pasa una fecha a años
            new \Twig_SimpleFilter('spaceencodeurl', array($this, 'spaceencodeurl')), //Convert a string to "slug" or "friendly"
            new \Twig_SimpleFilter('spacedecodeurl', array($this, 'spacedecodeurl')),//Convert a string to "slug" or "friendly"
            new \Twig_SimpleFilter('summarytextbywords', array($this, 'summarytextbywords')), //cut a text counted by words
            new \Twig_SimpleFilter('summarytextbychars', array($this, 'summarytextbychars')),//cut a text counted by chars
            new \Twig_SimpleFilter('getMonthName', array($this, 'getMonthName')),
            new \Twig_SimpleFilter('getDayname', array($this, 'getDayname'))
        );
    }


    public function getAge($date){
        if ($date==null) $date = new \Datetime();
        $today = new \Datetime();
        return $today->diff($date)->y;
    }
    
    //cut a text counted by words
    public function summarytextbywords($text, $numwords) {
        return $this->beinterface->summarytextbywords($text, $numwords);
    }
    
    //string to slug
    public function spaceencodeurl($url, $notilde=false) {
        return $this->beinterface->spaceencodeurl($url, $notilde);
    }

    public function spacedecodeurl($url) {
        return $this->beinterface->spacedecodeurl($url);
    }
    //cut a text counted by chars
    public function summarytextbychars($text, $numchars) {
        return $this->beinterface->summarytextbychars($text, $numchars);
    }    
    //get the month name in current locale
    public function getMonthName($number){
        return $this->beinterface->getMonthName(intval($number));
    }
    
    //get the day name in current locale
    public function getDayname($number, $shortmode=false){
        return $this->beinterface->getDayName(intval($number), $shortmode);
    }
    
    public function getName() {
        return 'beinterface_twig_extension';
    }

}
